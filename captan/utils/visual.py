import os
import numpy as np
from matplotlib import pyplot as plt

def plot_learning_curve(rnn, fname, met="loss"):
    """ Plot learning curve. """
    pth, ext = os.path.splitext(fname)
    gname = pth + (".%s" % met) + ext
    lt = rnn.history[met]
    lv = rnn.history["val_%s" % met]
    fig = plt.figure(figsize=(4, 3))
    plt.plot(lt, label="training")
    plt.plot(lv, label="validation")
    if met == "loss":
        j = np.argmin(lv)
        plt.plot(j, lv[j], ".", color="black")
    plt.ylabel(met.capitalize())
    plt.xlabel("Epochs")
    plt.legend()
    fig.tight_layout()
    plt.grid()
    plt.savefig(gname)
    plt.close()
    print("Written %s" % gname)
