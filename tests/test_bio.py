import numpy as np
import unittest
from collections import Counter
from captan.utils.bio import load_amino_acids, sequence_tensor, sequence_split_score, sequence_random_chop

class TestBio(unittest.TestCase):

    def test_sequence_tensor(self):
        """ Test representation of sequence as a tensor. """
        df = load_amino_acids()
        alphabet = df.index
        S = np.array(["LMF", "QES", "NDT"])
        T, _ = sequence_tensor(S, alphabet)
        assert T.shape == (3, 3, 20)
        assert T[0, 0, 2] == 1
        assert T[0, 1, 3] == 1
        assert T[0, 2, 4] == 1
        assert T[1, 0, 7] == 1
        assert T[1, 1, 8] == 1
        assert T[1, 2, 9] == 1
        assert np.all(T.sum(axis=(2, 1)) == 3)

    def test_sequence_split(self):
        """  Test sequence on score. """
        s = "ATPTTTPMM"
        y = [0, 0, 0, 1, 1, 1, 0, 1, 1]
        r_true = ["ATP", "TTT", "P", "MM"]
        q_true = [0, 1, 0, 1]
        r_test, q_test = sequence_split_score(s, y)
        assert tuple(r_test) == tuple(r_true)
        assert tuple(q_test) == tuple(q_true)
        s = "ATPTTTPMM"
        y = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        r_test, q_test = sequence_split_score(s, y)
        r_true = [s]
        q_true = [0]
        assert tuple(r_test) == tuple(r_true)
        assert tuple(q_test) == tuple(q_true)
        s = "ATPTTTPMM"
        y = [1, 1, 1, 1, 1, 1, 1, 1, 1]
        r_test, q_test = sequence_split_score(s, y)
        r_true = [s]
        q_true = [1]
        assert tuple(r_test) == tuple(r_true)
        assert tuple(q_test) == tuple(q_true)

    def test_sequence_random_chop(self):
        """ Test random chopping of sequences. """
        s = "ATP" * 1000
        n = len(s)
        rng = (3, 4, 5)
        probs = (.3, .5, .2)
        chops = sequence_random_chop(s, rng, probs)
        lns = list(map(len, chops))
        c = Counter(lns)
        for r, p in zip(rng, probs):
            assert np.abs(c[r]/len(lns) - p) < 0.05 * n


if __name__ == "__main__":
    unittest.main()
