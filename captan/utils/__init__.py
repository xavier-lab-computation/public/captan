
_AMINO_ACIDS = [
     {
       "Short": "G",
       "Full": "Glycine",
       "Three": "Gly",
       "Hydrophobicity": "neutral",
       "Class": "aliphatic"
     },
     {
       "Short": "A",
       "Full": "Alanine",
       "Three": "Ala",
       "Hydrophobicity": "neutral",
       "Class": "aliphatic"
     },
     {
       "Short": "L",
       "Full": "Leucine",
       "Three": "Leu",
       "Hydrophobicity": "hydrophobic",
       "Class": "aliphatic"
     },
     {
       "Short": "M",
       "Full": "Methionine",
       "Three": "Met",
       "Hydrophobicity": "hydrophobic",
       "Class": "sulfur"
     },
     {
       "Short": "F",
       "Full": "Phenylalanine",
       "Three": "Phe",
       "Hydrophobicity": "hydrophobic",
       "Class": "aromatic"
     },
     {
       "Short": "W",
       "Full": "Tryptophan",
       "Three": "Trp",
       "Hydrophobicity": "hydrophobic",
       "Class": "aromatic"
     },
     {
       "Short": "K",
       "Full": "Lysine",
       "Three": "Lys",
       "Hydrophobicity": "hydrophilic",
       "Class": "basic"
     },
     {
       "Short": "Q",
       "Full": "Glutamine",
       "Three": "Gln",
       "Hydrophobicity": "hydrophilic",
       "Class": "amidic"
     },
     {
       "Short": "E",
       "Full": "Glutamic Acid",
       "Three": "Glu",
       "Hydrophobicity": "hydrophilic",
       "Class": "acidic"
     },
     {
       "Short": "S",
       "Full": "Serine",
       "Three": "Ser",
       "Hydrophobicity": "neutral",
       "Class": "hydroxilic"
     },
     {
       "Short": "P",
       "Full": "Proline",
       "Three": "Pro",
       "Hydrophobicity": "neutral",
       "Class": "aliphatic"
     },
     {
       "Short": "V",
       "Full": "Valine",
       "Three": "Val",
       "Hydrophobicity": "hydrophobic",
       "Class": "aliphatic"
     },
     {
       "Short": "I",
       "Full": "Isoleucine",
       "Three": "Ile",
       "Hydrophobicity": "hydrophobic",
       "Class": "aliphatic"
     },
     {
       "Short": "C",
       "Full": "Cysteine",
       "Three": "Cys",
       "Hydrophobicity": "hydrophobic",
       "Class": "sulfur"
     },
     {
       "Short": "Y",
       "Full": "Tyrosine",
       "Three": "Tyr",
       "Hydrophobicity": "hydrophobic",
       "Class": "aromatic"
     },
     {
       "Short": "H",
       "Full": "Histidine",
       "Three": "His",
       "Hydrophobicity": "neutral",
       "Class": "basic"
     },
     {
       "Short": "R",
       "Full": "Arginine",
       "Three": "Arg",
       "Hydrophobicity": "hydrophilic",
       "Class": "basic"
     },
     {
       "Short": "N",
       "Full": "Asparagine",
       "Three": "Asn",
       "Hydrophobicity": "hydrophilic",
       "Class": "amidic"
     },
     {
       "Short": "D",
       "Full": "Aspartic Acid",
       "Three": "Asp",
       "Hydrophobicity": "hydrophilic",
       "Class": "acidic"
     },
     {
       "Short": "T",
       "Full": "Threonine",
       "Three": "Thr",
       "Hydrophobicity": "neutral",
       "Class": "hydroxilic"
     }
    ]