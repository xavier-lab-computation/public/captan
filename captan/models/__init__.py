
# Model configurations
CONFIGS = {
    "Lstm_iweight_c128x64_32x48x4": {"init": {"hidden": 32, "filters": 48, "filters_depth": 4,
                                           "dropout": 0.1, "activation": "tanh", "conversion": None,
                                           "bidirectional": True, "optimizer": "Adam", "callback": True,
                                           "conv_kernel": 20, "conv_filters": 128,
                                           "n_threads": 2, "reweight": True},
                                           "fit": {"epochs": 5, "verbose": True, "n_batches": 112}},
}
