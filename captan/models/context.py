import numpy as np
import keras
import tensorflow as tf
from time import time
from sklearn.cluster import KMeans
from sklearn.metrics import roc_auc_score, average_precision_score
from captan.utils.bio import string_tensor, sequence_tensor


class LSTMeta:

    """
        LSTM neural network model with sequence and metadata input.
    """

    # Constants
    FAMILY_LSTM = "lstm"

    def __init__(self, alphabet, d_in=1, d_out=4, hidden=16, filters=8, filters_depth=1,
                 activation="tanh", conversion=None, bidirectional=False,
                 optimizer="Adam", callback=True, family=FAMILY_LSTM,
                 conv_kernel=4, conv_filters=10,
                 conv_kernel_task=4, conv_filters_task=10,
                 dropout=0, layer_norm=False,
                 reweight = False, n_threads=None, **rnn_args):

        # Input parameters
        self.d_in = d_in
        self.d_out = d_out
        self.dropout = dropout
        self.alphabet = alphabet
        self.a = len(alphabet)
        self.activation = activation
        self.hidden = hidden
        self.filters = filters
        self.filters_depth = filters_depth
        self.conversion = conversion
        self.bidirectional = bidirectional
        self.optimizer = optimizer
        self.callback = callback
        self.rnn_args = rnn_args
        self.conv_kernel = conv_kernel
        self.conv_filters = conv_filters
        self.conv_filters_task = conv_filters_task
        self.conv_kernel_task = conv_kernel_task
        self.layer_norm = layer_norm
        self.reweight = reweight
        self.family = family

        # State parameters
        self.model = None
        self.attention_model = None
        self.embedding_model = None
        self.embedding_dim = None
        self.motif_model = None
        self.history = dict()
        self.best_score = np.inf
        self.best_weights = None
        self.total_epochs = 0
        self.best_total_epoch = 0
        self.set_devices(n_threads)
        self.architecture()

    @staticmethod
    def set_devices(n_threads):
        """ Check devices and set GPU if available. """
        if n_threads is not None:
            try:
                tf.config.threading.set_inter_op_parallelism_threads(n_threads)
                tf.config.threading.set_intra_op_parallelism_threads(n_threads)
            except:
                print("Using default Keras/TF session parameters.")
        for dev in tf.config.get_visible_devices():
            ite = tf.config.threading.get_inter_op_parallelism_threads()
            ita = tf.config.threading.get_intra_op_parallelism_threads()
            # print(dev)
            # print("Inter threads: %s" % ite)
            # print("Intra threads: %s" % ita)


    def architecture(self):
        """ Define model architecture. """
        d_in = self.d_in
        d_out = self.d_out
        a = self.a
        hidden = self.hidden
        filters = self.filters
        activation = self.activation
        bidirectional = self.bidirectional
        optimizer = self.optimizer
        conv_kernel = self.conv_kernel
        conv_filters = self.conv_filters
        rnn_args = self.rnn_args
        dropout = self.dropout
        filters_depth = self.filters_depth
        layer_norm = self.layer_norm
        family = self.family

        # Inputs
        inp_seq = keras.layers.Input(shape=(None, a), name="Input_seq")
        inp_meta = keras.layers.Input(shape=(None, d_in), name="Input_meta")
        emb = inp_seq

        # Pre-training kernels
        conv = emb = keras.layers.Conv1D(kernel_size=conv_kernel,
                                         filters=conv_filters,
                                         name="Convolution_pre",
                                         padding="same")(emb)
        self.motif_model = keras.Model(inputs=[inp_seq], outputs=conv)

        ### Task-specific kernels
        rnn_class = keras.layers.LSTM
        curr = keras.layers.Concatenate(name="Conv_and_meta")([emb, inp_meta])
        if family == self.FAMILY_LSTM:
            for li in range(2):
                # Phase 2
                forward = rnn_class(hidden,
                                    activation=activation,
                                    name="Pretrain_LSTM_forward_%02d" % li,
                                    return_sequences=True,
                                    **rnn_args)(curr)
                if bidirectional:
                    backward = rnn_class(hidden,
                                         go_backwards=True,
                                         activation=activation,
                                         name="Pretrain_LSTM_back_%02d" % li,
                                         return_sequences=True,
                                         **rnn_args)(curr)
                    curr = keras.layers.Concatenate()([forward, backward])
                curr = keras.layers.LayerNormalization()(curr) if layer_norm else curr

        # Smooth combination of presentation and allele-specific branches.
        filt = keras.layers.Dense(filters,
                                  name="Pretrain_decision",
                                  activation="relu")(curr)

        # Shared for pre-training or task-specific
        edim = 0
        for i in range(filters_depth):
            edim = int(filters / (1.2 ** i))
            filt = keras.layers.Dropout(rate=dropout // 1.5 ** i)(filt) if dropout else filt
            filt = keras.layers.Dense(edim,
                                      activation="relu",
                                      name="Filter_%02d" % i)(filt)
        out = keras.layers.Dense(d_out, activation="sigmoid", name="Final_decision")(filt)

        # Embedding models
        self.embedding_model = keras.Model(inputs=[inp_seq, inp_meta], outputs=filt)
        self.embedding_dim = edim

        # Compile model
        model = keras.Model(inputs=[inp_seq, inp_meta], outputs=out)
        model.compile(optimizer=optimizer,
                      loss=keras.metrics.binary_crossentropy,
                      metrics=[],
                      sample_weight_mode='temporal')
        self.model = model

    def evaluate(self, T, X, Z):
        """
        Evaluate loss for given data
        fitting goes through multiple epochs.
        :param T: Tensor of shape (n, length, a).
        :param X: Tensor of shape (n, length, d).
        :param Z: Tensor of shape (n, length, 1).
        """
        model = self.model
        loss = model.evaluate([T, X], Z, verbose=False)[0]
        return loss

    def evaluate_batches_loss(self, B):
        """
        Evaluate loss for given data
        fitting goes through multiple epochs.
        :param B: Batches of training data.
        """
        model = self.model
        loss = 0
        t = time()
        for T, X, Z, W in B:
            loss += model.evaluate([T, X], Z, sample_weight=W, verbose=False)
        loss = loss / len(B)
        t = time() - t
        print("Evaluating %d batches in %.2f s" % (len(B), t))
        return loss

    def evaluate_batches_metrics(self, B):
        """
        Evaluate loss for given data
        fitting goes through multiple epochs.
        :param B: Batches of training data.
        """
        model = self.model
        y = []
        f = []
        t = time()
        for T, X, Z, W in B:
            f.extend(model.predict([T, X], verbose=False).ravel())
            y.extend(Z.ravel())
        auc = roc_auc_score(y_true=np.array(y).astype(bool).astype(int), y_score=np.array(f))
        avp = average_precision_score(y_true=np.array(y).astype(bool).astype(int), y_score=np.array(f))
        t = time() - t
        print("Evaluating %d batches in %.2f s (metrics)" % (len(B), t))
        return auc, avp

    def lists2tensors(self, S, X, Y, prior = 0):
        """ Convert lists to tensors and add sample weights.
            This is a single batch. Re-weight to match probability distribution correctly."""
        assert 0 <= prior < 1
        alphabet = self.alphabet
        conversion = self.conversion
        d_in = self.d_in
        d_out = self.d_out
        Ts, W = sequence_tensor(S, alphabet=alphabet, conversion=conversion)
        Tx = np.zeros((Ts.shape[0], Ts.shape[1], d_in))
        Z = np.zeros((Ts.shape[0], Ts.shape[1], d_out))
        for i, y in enumerate(Y):
            Tx[i, :len(y), :] = X[i]
            Z[i, :len(y), :] = y
            if prior > 0:
                hits = y.sum(axis=-1).astype(bool).astype(float)
                minus = hits == 0                                                                # zeros
                # plus = hits > 0                                                                # positives
                W[i, :len(y)][minus] = (1 - prior) * len(y) / np.maximum(1, np.sum(minus))       # re-weight zeros
                # W[i, :len(y)][plus] = prior * len(y) / np.maximum(1, np.sum(plus))             # re-weight positives
        return Ts, Tx, Z, W

    def batches(self, S, X, Y, n_batches=None, reweight = True):
        """ Split data in a number of batches.
            Compute the percentage of inserted padded sequences.
        """
        assert isinstance(S, list)
        assert isinstance(Y, list)
        Sa = np.array(S)
        Ya = np.array(Y)
        Xa = np.array(X)
        N = len(S)
        L = np.fromiter(map(len, S), dtype=int, count=N).reshape((-1, 1))


        # Determine prior
        prior = 0
        if reweight:
            hits = np.sum([np.sum(y) for y in Y])
            total = np.sum([np.product(y.shape) for y in Y])
            prior = hits / total
            print("Setting prior: %.3f (%d out of %d in % d sequences)" %
                  (prior, int(hits), int(total), len(Y)))

        # Set batches based on length
        if n_batches is None:
            Z = np.arange(N)
            print("Treating each sequence as a single batch.")
        else:
            Z = KMeans(n_clusters=n_batches, random_state=42).fit_predict(L)

        # Assemble batches
        B = list()
        for z in sorted(set(Z)):
            inxs = np.ravel(Z) == z
            Si = Sa[inxs]
            Xi = Xa[inxs]
            Yi = Ya[inxs]
            St, Xt, Yt, Wt = self.lists2tensors(Si, Xi, Yi, prior=prior)
            min_len = float(np.min(L[inxs]))
            mean_len = float(np.mean(L[inxs]))
            total_steps = St.shape[1]
            total_diff = float(np.sum(np.max(L[inxs]) - L[inxs]))
            B.append((St, Xt, Yt, Wt))
            if n_batches is not None and (z < 10 or z == n_batches - 1):
                print("Batch %d, size %d. Length min: %d mean: %d, max: %d, padded: %d (%.2f %%), positives: %d,  %d (%.2f %%)" %
                      (z, len(St), min_len, mean_len, total_steps, total_diff,
                       100.0 * total_diff / (St.shape[0] * St.shape[1]),
                       Yt.sum(axis=(-2, -1)).astype(bool).sum(), Yt.sum(), 100.0 * Yt.mean()))
        return B

    @staticmethod
    def split(S, X, Y, split=.2):
        """ Split data set by fraction of validation.
            :param S: List of tensors of shape (n, length, a).
            :param X: List of tensors of shape (n, length, d).
            :param Y: List of tensors of shape (n, length, 1).
            :param split: Fraction of validation data points.
        """
        N = len(S)
        inxs = np.arange(N)
        np.random.shuffle(inxs)
        b = int(N * (1 - split))
        tr = np.sort(inxs[:b])
        va = np.sort(inxs[b:])
        S_tr = [S[i] for i in tr]
        X_tr = [X[i] for i in tr]
        Y_tr = [Y[i] for i in tr]
        S_va = [S[i] for i in va]
        X_va = [X[i] for i in va]
        Y_va = [Y[i] for i in va]
        assert len(set(tr).intersection(set(va))) == 0
        assert len(tr) > 0
        assert len(va) > 0
        return (S_tr, X_tr, Y_tr), (S_va, X_va, Y_va)

    @staticmethod
    def get_weights(model):
        """ Weight getter. """
        return [layer.get_weights() for layer in model.layers]

    @staticmethod
    def set_weights(model, weights):
        """ Weight setter (in place). """
        L = len(model.layers)
        for j in range(L):
            model.layers[j].set_weights(weights[j])

    def fit(self, S, X, Y, epochs=10, validation_split=None,
            validation_data = None, verbose=False, n_batches=None, **kwargs):
        """
        Fit model. Need to implement own callback mechanism because
        fitting goes through multiple epochs.
        :param S: Array of strings of shape (n,).
        :param X: List of tensors of shape (n, length, d).
        :param Y: List of tensors of shape (n, length, k).
        :param epochs: Number of epochs where each sequence is passed through a model.
        :param n_batches: Number of batches.
        :param validation_split: Fraction of validation data points.
        :param validation_data: Fixed validation data in the same shape.
        :param verbose: Verbose.
        :return:
        """
        model = self.model
        callback = self.callback
        best_score = self.best_score
        best_weights = self.best_weights
        best_total_epoch = self.best_total_epoch
        total_epochs = self.total_epochs
        reweight = self.reweight
        assert "epochs" not in kwargs
        assert "batch_size" not in kwargs
        assert model is not None
        history = dict()
        B_va = None, None, None
        S_va, X_va, Y_va = None, None, None
        if validation_data is not None:
            S_va, X_va, Y_va = validation_data
        elif validation_split is not None:
            (S, X, Y), (S_va, X_va, Y_va) = self.split(S, X, Y, validation_split)
        if S_va is not None:
            B_va = self.batches(S_va, X_va, Y_va, reweight=reweight, n_batches=n_batches)
            if verbose: print("Scoring on %d sequences" % len(S_va))
        if verbose: print("Training on %d sequences" % len(S))
        B = self.batches(S, X, Y, reweight=reweight, n_batches=n_batches)
        best_current_epoch = -1
        for e in range(epochs):
            # Sample batch before each epoch
            Sxs = self.split(S, X, Y, 0.5)
            t1 = time()
            for s in range(2):
                Be = self.batches(*Sxs[s], n_batches=n_batches, reweight=reweight)
                for i, (Tb, Xb, Zb, Wb) in enumerate(Be):
                    total_epochs += 1
                    model.fit([Tb, Xb], Zb, epochs=1, batch_size=len(Tb),
                              sample_weight=Wb, verbose=False, **kwargs)
            loss = self.evaluate_batches_loss(B)
            auc, avp = self.evaluate_batches_metrics(B)
            history["loss"] = history.get("loss", []) + [loss]
            history["time"] = history.get("time", []) + [time() - t1]
            history["auc"] = history.get("auc", []) + [auc]
            history["avp"] = history.get("avp", []) + [avp]
            val_loss = score = val_auc = val_avp = np.nan
            if B_va is not None:
                val_loss = score = self.evaluate_batches_loss(B_va)
                val_auc, val_avp = self.evaluate_batches_metrics(B_va)
                history["val_loss"] = history.get("val_loss", []) + [val_loss]
                history["val_auc"] = history.get("val_auc", []) + [val_auc]
                history["val_avp"] = history.get("val_avp", []) + [val_avp]
            status = "none"
            if score < best_score:
                best_total_epoch = total_epochs
                best_score = score
                best_weights = self.get_weights(model)
                best_current_epoch = e
                status = "improved"
            print("Epoch %d/%d (%d), loss : %.5f, val. loss : %.5f (%s), val. auc: %.3f, avp: %.3f, %.2f s" %
                  (e, epochs, total_epochs, loss, val_loss, status, val_auc, val_avp, time() - t1))
            if (callback and best_current_epoch >= 0) or \
                    (callback and self.best_weights is not None) or \
                    (e == epochs - 1):
                self.set_weights(model, best_weights)
                if verbose:
                    print("Setting best weights from epoch %d with loss: %.5f" % (best_total_epoch, best_score))

        # Update global trackers
        self.total_epochs = total_epochs
        self.best_total_epoch = best_total_epoch
        self.best_score = best_score
        self.best_weights = best_weights
        for ky in history.keys():
            self.history[ky] = self.history.get(ky, []) + history[ky]

    def predict(self, S, X):
        """ Predict for each sequence independently. """
        alphabet = self.alphabet
        conversion = self.conversion
        model = self.model
        d_out = self.d_out
        Yp = list()
        for s, x in zip(S, X):
            St = string_tensor(s, alphabet=alphabet, conversion=conversion)
            Xt = x.reshape(1, x.shape[0], x.shape[1])
            yp = np.round(model.predict([St, Xt], verbose=False), 4)
            yp = yp.reshape((len(s), d_out))
            Yp.append(yp)
        return Yp

    def predict_embedding(self, S, X):
        """ Predict values for motifs. """
        alphabet = self.alphabet
        conversion = self.conversion
        embedding_model = self.embedding_model
        embedding_dim = self.embedding_dim
        M = []
        for s, x in zip(S, X):
            St = string_tensor(s, alphabet=alphabet, conversion=conversion)
            Xt = x.reshape(1, x.shape[0], x.shape[1])
            my = np.round(embedding_model.predict([St, Xt]), 4).reshape((len(s), embedding_dim))
            M.append(my)
        return M

    def predict_motifs(self, S):
        """ Predict values for motifs. """
        alphabet = self.alphabet
        conversion = self.conversion
        motif_model = self.motif_model
        conv_filters = self.conv_filters
        M = []
        for s in S:
            St = string_tensor(s, alphabet=alphabet, conversion=conversion)
            my = np.round(motif_model.predict([St]), 4).reshape((len(s), conv_filters))
            M.append(my)
        return M


