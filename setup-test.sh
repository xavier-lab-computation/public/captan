#!/bin/bash
# Test setup for provided protein set.
MODEL_CORE_DIR=./models/models_core/
MODEL_CTX_DIR=./models/models_context/
IN_FA=./examples/hcov/fa/hcov.faa
OUT=./examples/hcov/captan/
mkdir -p $OUT

echo "Start"
date

# Test core
echo captan-core --models $MODEL_CORE_DIR --input $IN_FA --output $OUT --alleles "DRB1*11:01" "DRB1_03_01" "DQA1*01:03,DQB1*06:03" --lengths 20 --details
time captan-core --models $MODEL_CORE_DIR --input $IN_FA --output $OUT --alleles "DRB1*11:01" "DRB1_03_01" "DQA1*01:03,DQB1*06:03" --lengths 20 --details 1>$OUT.captan_core.out.txt 2>$OUT.captan_core.err.txt
echo "Status:" "$?"

# Test context
echo captan-context --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --isotypes "DR" "DQ" "DP" --threshold 0.2 --lengths 20 --details
time captan-context --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --isotypes "DR" "DQ" "DP" --threshold 0.2 --lengths 20 --details 1>$OUT.captan_ctx.out.txt 2>$OUT.captan_ctx.err.txt
echo "Status:" "$?"

# Test merge
echo captan-merge --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --details --lengths 20
time captan-merge --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --details --lengths 20 1>$OUT.captan_mrg.out.txt 2>$OUT.captan_mrg.err.txt
echo "Status:" "$?"

echo "End"
date