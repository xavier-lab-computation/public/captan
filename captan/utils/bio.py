import itertools
import os
import numpy as np
import pandas as pd
from Bio.Seq import Seq
from Bio.SeqIO import parse, write
from Bio.SeqRecord import SeqRecord
from Bio.SubsMat import MatrixInfo
from intervaltree import IntervalTree, Interval
from captan.utils import _AMINO_ACIDS

pmbec_dict = None


def load_amino_acids():
    """ Load amino-acid matrix. """
    df = pd.DataFrame(_AMINO_ACIDS)
    df.set_index("Short", drop=True, inplace=True)
    return df

# Initialize conversion variables
_conv = dict()
_conv_current = ""
_position = None
def load_string_conversion(alphabet, conversion):
    """ Load conversion dictionary into the global scope.
        Not sensitive to switching alphabets.
    """
    global _conv
    global _conv_current
    global _position
    if conversion == _conv_current:
        return
    _conv_current = conversion
    _position = dict(((a, i) for i, a in enumerate(alphabet)))
    if conversion is not None:
        _conv = dict()
        print("Converting sequences using %s" % conversion)
        if conversion in MatrixInfo.available_matrices:
            mx = getattr(MatrixInfo, conversion)
            for a in sorted(list(_position.keys())):
                _conv[a] = np.array([mx[a, b] if (a, b) in mx else mx[b, a]
                                    for b, j in sorted(_position.items(), key=lambda t: t[1])])
        elif conversion == "PMBEC":
            raise NotImplementedError
        else:
            raise NotImplementedError


def string_tensor(s, alphabet, conversion=None):
    """ Convert a single string to a tensor training example. """
    global _conv
    global _position
    load_string_conversion(alphabet, conversion)
    T = np.zeros((1, len(s), len(alphabet)))
    if conversion is None:
        pc = ((0, i, _position.get(c)) for i, c in enumerate(s) if c in _position)
        pz = tuple(zip(*pc))
        T[pz] = 1
    else:
        V = np.vstack([_conv[a] for a in s if a in _position])
        T[0, :V.shape[0], :] = V
    return T


def sequence_tensor(S, alphabet, conversion=None, length=None):
    """
     Convert strings to tensor.
    :param S: Array of strings to tensor for a given alphabet.
    :param alphabet: Sets of characters.
    :param conversion: Use amino acid similarity matrix as a feature rep.
        https://biopython.org/DIST/docs/api/Bio.SubsMat.MatrixInfo-module.html
    :param length: Pre-determined length.
    :return: Tensor representation.
    """
    m = max(map(len, S))
    if length is not None:
        assert m <= length
    length = m if length is None else length
    T = np.zeros((len(S), length, len(alphabet)))
    W = np.zeros((len(S), length))
    for si, s in enumerate(S):
        T[si, :len(s), :] = string_tensor(s, alphabet, conversion)
        W[si, :len(s)] = 1
    return T, W


def sequence_split(s, g):
    """ Split the sequence s on substring g (return left and right flank).

        >>> s = "KQTSSLPSASHFSQLSCMPSLIAQQQQNPQVYVSQSAAAQIPAFY"
        >>> g = "QQQQNPQ"
    """
    if g not in s:
        return None
    return s.split(g)

def sequence_multi_split(s, gs):
    """ Split a sequence by multiple potentially overlapping epitopes.
        It is essential that sequence remain in multiple parts.

        >>> s = "KQTSSLPSASHFSQLSCMPSLIAQQQQNPQVYVSQSAAAQIPAFY"
        >>> g = "QQQQNPQ"
        >>> h = "SASHFSQ"
        >>> i = "QQNPQVY"
        >>> gs = [g, h, i]
    """
    it = IntervalTree()
    it.add(Interval(0, len(s)))
    for g in gs:
        if g in s:
            i = s.index(g)
            it.chop(i, i+len(g))
    return [s[i[0]:i[1]] for i in sorted(it.items())]

def sequence_chop(s, k):
    """ Chop the sequence into subsequences of length k.
        If s is not divisible by k, cutoff remaining sequences.
    """
    start = len(s) % k
    t = s[start:]
    return [t[i * k : (i + 1) * k] for i in range(len(t)//k)]

def sequence_chop_overlap(s, k):
    """ Chop the sequence into len(s) - k + 1 overlapping subsequences.
        >>> s = 'ATPTTTPMM'
    """
    lst = zip(*(s[i:] for i in range(k)))
    return list(map(lambda l: "".join(l), lst))


def sequence_multi_chop(S, k):
    """ Chop a list of sequences individually. """
    res = []
    for s in S:
        chp = sequence_chop(s, k)
        res.extend(chp)
    return res


def sequence_random_chop(s, rng=(2, 3), probs=(.5, .5)):
    """ Chop the sequence into subsequences of length within range.
        Potential remainder is discarded.
        :param s: string sequence.
        :param rng: length possibilities.
        :param probs: length probabilities.

        >>> s = 'ATPTTTPMM'
    """
    total = 0
    chops = []
    while total < len(s):
        k = np.random.choice(rng, p=probs)
        chunk = s[total:total+k]
        if len(chunk) in rng:
            chops.append(chunk)
        total = total + k
    return chops


def sequence_split_score(s, y):
    """ Split sequence into multiple parts based on a score vector.
        Return list of sequences and list of scores.
        >>> s = "ATPTTTPMM"
        >>> y = [0,0,0,1,1,1,0,1,1]
    """
    locs = np.where(np.array(y))[0]
    it = IntervalTree()
    for l in locs:
        it.add(Interval(l, l+1))
    it.merge_overlaps(strict=False)
    neg = IntervalTree(())
    neg.add(Interval(0, len(s)))
    for i in it:
        neg.chop(i[0], i[1])
    zn = zip(neg.all_intervals, [0] * len(neg.all_intervals))
    zp = zip(it.all_intervals, [1] * len(it.all_intervals))
    zs = sorted(list(zn) + list(zp))
    seqs = list((s[z[0][0]:z[0][1]] for z in zs))
    scores = [z[1] for z in zs]
    return seqs, scores

def sequence_rectify(s, letters, replacement="X"):
    """ Rectify sequence to contain only valid letters from a biological alphabet."""
    l_set = set(letters)
    return "".join(map(lambda c: c if c in l_set else replacement, s))


def fasta2numpy(in_fasta, max_lines = None, include_ids=False, max_length=None):
    """ Read fasta file to numpy array. """
    recs = parse(in_fasta, format="fasta")
    keep = lambda s: (max_length is None) or (len(s) <= max_length)
    if include_ids:
        ids, seqs = zip(*[(r.id, str(r.seq)) for r in recs if keep(r.seq)])
        arr = np.array(seqs)
        if max_lines is not None:
            arr = arr[:max_lines]
            ids = ids[:max_lines]
        return ids, arr
    else:
        arr = np.array([str(r.seq) for r in recs if keep(r.seq)])
        if max_lines is not None:
            arr = arr[:max_lines]
        return arr


def write_fasta(fname, data, col_name="sequence", mode="w"):
    """ Write a FASTA file either from a dict of pd.DataFrame ."""
    fp = open(fname, mode=mode)
    records = []
    if isinstance(data, dict):
        records = [SeqRecord(seq=Seq(val), id=str(ky), description="") for ky, val in data.items()]
    elif isinstance(data, pd.DataFrame):
        records = [SeqRecord(seq=Seq(row[col_name]), id=str(ky), description="")
                   for ky, row in data.iterrows()]
    write(handle=fp, sequences=records, format="fasta")
    return len(records)


def read_fasta(fname):
    """ Read a FASTA file into a dict. """
    return dict([(rec.id, str(rec.seq)) for rec in parse(open(fname), "fasta")])


def yield_fasta(fname):
    """ Read a FASTA file into a generator. """
    return ((rec.id, str(rec.seq)) for rec in parse(open(fname), "fasta"))


def motif_cross_entropy(x, m):
    """
    Compute cross-entropy between motif matrix and sequence in tensor form.
    :param x: Sequence converted to matrix.
    :param m: Motif matrix.
    :return: Cross-entropy loss.
    """
    f = x * m.values
    return - np.sum(np.log(f[f > 0]))


def motif_extract(s, m):
    """ Extract motif given by matrix m from sequence s.
        :param s: Sequence of amino acids.
        :param m: Motif data frame with amino acids as columns.
        :return: Best subsequence and cross-entropy loss.
    """
    l = m.shape[0]
    assert len(s) >= l
    t = string_tensor(s, alphabet=list(m.columns), conversion=None)[0]
    best_loss = np.inf
    best_seq = ""
    best_i = 0
    for i in range(len(s) - l + 1):
        x = t[i:i+l, :]
        e = motif_cross_entropy(x, m)
        if e < best_loss:
            best_loss = e
            best_seq = s[i:i+l]
            best_i = i
    return best_seq, best_loss, best_i


def load_motifs(model_dir, L, alleles=None):
    """ Load motif matrices for alleles. """
    motif_cache = dict()
    in_motifs = os.path.join(model_dir, "motifs.csv")
    if os.path.exists(in_motifs):
        aa = load_amino_acids()
        mf = pd.read_csv(in_motifs)
        pos = ["P%d"% p for p in range(1, L + 1)]
        count = 0
        for _, r in mf.iterrows():
            allele = r["Allele"]
            cls = r["Order"]
            if alleles is not None and allele not in alleles:
                continue
            wf = pd.DataFrame(index=pos, columns=aa.index, data=np.zeros((L, len(aa.index))))
            for p, a in itertools.product(pos, aa.index):
                wf.loc[p, a] = r["%s_%s" % (p, a)]
            if allele not in motif_cache:
                motif_cache[allele] = dict()
            motif_cache[allele][cls] = wf
            count += 1
        print("Loaded %d motifs for %d alleles" % (count, len(motif_cache)))
    return motif_cache
