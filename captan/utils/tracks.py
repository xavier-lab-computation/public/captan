import numpy as np
from scipy.stats import scoreatpercentile


# Constants
REG_NFLANK = "nflank"
REG_CFLANK = "cflank"
REG_FIRST = "first"
REG_SECOND = "second"
REG_SHARED = "shared"


def quantile(x, p=(5, 25, 50, 75, 95), prefix=""):
    """ Length quantiles.
        >>> x = np.random.randint(0, 20, size=30)
        >>> quantile(x)
    """

    pc = lambda q: scoreatpercentile(a=x, per=q)
    return dict([("%sp%0d" % (prefix, pi), pc(pi)) for pi in p])

def cut(x, pc):
    """ Cut vector into breaks based on quantile dict.
        >>> x = np.random.randint(0, 20, size=30)
        >>> pc = quantile(x)
        >>> list(zip(x, cut(x, pc)))
    """
    def find(xi, brks):
        hits = [bi for bi, b in enumerate(brks) if b[0] <= xi < b[1]]
        return hits[0] if len(hits) else None
    items = sorted(pc.items(), key=lambda p: p[1])
    # keys = [i[0] for i in items]
    vals = [i[1] for i in items]
    keys = [-np.inf] + list(np.round(vals, 2)) + [np.inf]
    vals = [-np.inf] + vals + [np.inf]
    # keys = ["-inf"] + keys + ["inf"]
    breaks = list(zip(vals, vals[1:]))
    labels = list(zip(keys, keys[1:]))
    inxs = [find(xi, breaks) for xi in x ]
    cuts = [labels[j] if j is not None else None for j in inxs]
    return cuts


def transitions(x):
    """ Find nonzero intervals. """
    nz = np.nonzero(x)[0]
    ints = []
    if len(nz) == 0:
        return ints
    a, b = nz[0], nz[0]
    for n in nz[1:]:
        if n - b <= 1:
            b = n
        else:
            ints.append((a, b + 1))
            a, b = n, n
    ints.append((a, b + 1))
    return ints


def expand(ints, flank=5, L=np.inf):
    """
    Expand regions for a number of positions up- and downstream. Do not overlap
    with existing intervals.
    :param ints: Interval list
    :param flank: Size of added flanking positions
    :param L: Maximum length bound.
    :return Extended intervals.
    """
    ints = [(0, 0)] + sorted(ints) + [(L, L)]
    news = []
    for (_, a), (b, c), (d, _) in zip(ints, ints[1:], ints[2:]):
        bn = max(a, b - flank)
        cn = min(d, c + flank)
        news.append((bn, cn))
    return news


def overlaps(X, flank=5, offset=0):
    """ List overlaps between two binary tracks. Extend individual tracks first and merged.
            Types of regions:
                0 Flanking, specific 0, specific 1, shared.
    """
    res = dict()
    assert X.shape[0] == 2
    Y = X.astype(bool).astype(float)
    z = Y.sum(axis=0)
    nf = np.zeros((len(z)), )
    cf = np.zeros((len(z)),)
    for a, b in transitions(z):
        ai = max(0, a - offset)
        aj = max(0, a - offset - flank)
        bi = min(len(z), b + offset)
        bj = min(len(z), b + offset + flank)
        nf[aj : ai] = 1 * (z[aj : ai] == 0)
        cf[bi : bj] = 1 * (z[bi : bj] == 0)
    cflanks = transitions(cf)
    nflanks = transitions(nf)
    first = transitions(X[0] > X[1])
    second = transitions(X[0] < X[1])
    shared = transitions(z == 2)
    for s in nflanks: res[s] = REG_NFLANK
    for s in cflanks: res[s] = REG_CFLANK
    for s in first: res[s] = REG_FIRST
    for s in second: res[s] = REG_SECOND
    for s in shared: res[s] = REG_SHARED
    return sorted([(k, v) for k, v in res.items()])


def slice_tensor(X, window, stretch=50):
    """ Slice tensor into sliding windows. """
    L = X.shape[1]
    Z = np.zeros((L, window, X.shape[2]))
    for j in range(L):
        shift = min(j + stretch, L)
        Z[j, :shift - j] = X[0, j:shift, :]
    return Z


def local_maxima(Y, length=11, impute=False, convolve=True):
    """ Extract local maxima from prediction as a matrix on ones and nans
        (for plotting). In case of equal values in a window, choose first one.
    """
    N, M = Y.shape
    Yc = np.zeros(Y.shape)
    Ym = np.zeros(Y.shape)
    for i in range(N):
        yc = np.convolve(Y[i], np.ones((length,)) / length, mode="same") if convolve else Y[i]
        if impute:
            yc[:length//2] = Y[i][:length//2]
            yc[-length//2:] = Y[i][-length//2:]
        Yc[i, :] = yc
        for j in range(M):
            start = max(0, j - length)
            end = min(j + length, M)
            free = np.sum(Ym[i, start:end]) == 0
            Ym[i, j] = free and (Yc[i, j] == np.max(Yc[i, start:end]))
    return Yc, Ym


def convolve_op(x, a=5, b=20, op=np.mean):
    """
    Local convolution in interval [a, b] with a selected aggregation operator.
    Offset predictions in x by taking the maximum/mean over the neighbouring positions.
    :param x: Input vector.
    :param a: Left offset.
    :param b: Right offset.
    :param op: Aggregating operation.
    :return: Convolved vector.
    """
    y = np.zeros((len(x),))
    for i in range(len(y)):
        start = max(0, i - a)
        end = min(len(x), i + b)
        y[i] = op(x[start:end])
    return y