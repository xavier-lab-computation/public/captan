import keras
import tensorflow as tf

LENGTH = 50
REDUCE_SUM = "sum"
REDUCE_MAX = "max"


def architecture(d, p, reverse=False, l1=0, embed_dim=None,
                 kernel_size=7, kernel_dim=16,
                 filter_dim=16, dropout=0.2, reduce=REDUCE_SUM, pool=True, restart=0):
    """
    :param d: Embedding size.
    :param p: Number of classes.
    :param embed_dim: Embedding dimension for input features.
    :param kernel_dim: Number of kernels.
    :param kernel_size: Size of kernel.
    :param filter_dim: Dimension of final embedding.
    :param reverse: Process sequence in reverse order.
    :param dropout: Dropout rate.
    :param l1: L1 regularization.
    :param reduce: Sum or average.
    :param pool: Use max pooling.
    :param restart: Dummy parameter for random restarts.
    :return: Classification model.
    """
    assert restart is not None

    # Generic layers
    Conv = keras.layers.Conv1D(kernel_dim,
                               kernel_size=kernel_size,
                               kernel_regularizer=keras.regularizers.l1(l1) if l1 else None,
                               kernel_constraint=keras.constraints.NonNeg(),
                               padding="valid",
                               use_bias=False)
    Conv2 = keras.layers.Conv1D(kernel_dim, kernel_size=kernel_size)
    Pool = keras.layers.MaxPool1D(pool_size=kernel_size, strides=kernel_size // 2)
    Revert = keras.layers.Lambda(lambda x: tf.reverse(x, axis=(1,)), name="Revert")

    # Assemble model
    inp = keras.layers.Input(shape=(None, d))
    forward = keras.layers.Dense(embed_dim, activation="linear")(inp) if embed_dim is not None else inp
    fout = Conv(forward)
    fout = Conv2(fout)
    fout = Pool(fout) if pool else fout
    if reverse:
        backward = Revert(forward)
        bout = Revert(Conv2(Conv(backward)))
        bout = Pool(bout)
        curr = keras.layers.Concatenate()([bout, fout])
    else:
        curr = fout
    hid = keras.layers.Dense(filter_dim, activation="relu")(curr)
    hid = keras.layers.Dense(filter_dim // 2, activation="relu")(hid)
    hid = keras.layers.Dropout(rate=dropout)(hid) if dropout > 0 else hid
    if reduce == REDUCE_SUM:
        hid = keras.layers.Lambda(lambda x: tf.reduce_sum(x, axis=(1,)),
                                  name="Reduce_%s" % reduce)(hid)
    elif reduce == REDUCE_MAX:
        hid = keras.layers.Lambda(lambda x: tf.reduce_max(x, axis=(1,)),
                                  name="Reduce_%s" % reduce)(hid)
    out = keras.layers.Dense(p, activation="sigmoid")(hid)
    model = keras.Model(inputs=inp, outputs=out)
    model.compile(loss=keras.losses.binary_crossentropy,
                  optimizer=keras.optimizers.Adam(),
                  metrics=[keras.metrics.AUC()])
    return model
