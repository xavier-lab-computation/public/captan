# Common utils for binary files
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import pandas as pd
import tensorflow as tf
from captan.utils.bio import write_fasta

# Verbosity levels for binaries
tf.get_logger().setLevel('ERROR')
tf.autograph.set_verbosity(0)

# Computation of the maxima
BUFFER = 10000
STRETCHES = [7, 18, 25, 50]

# Constants
CONFIG_CORE = "CAPTAn_core"
CONFIG_CONTEXT = "CAPTAn_ctx"
CONFIG_MERGED = "CAPTAn"


def allele_ext(a):
    """ Rename alleles to external format."""
    return a.replace("*", "_").replace(":", "_").replace(",", "__")


def write_out(rows, out_dir, pep_dir, out_raw=False, first=True, config=CONFIG_CORE):
    """ Write current buffer of rows. """
    fname = os.path.join(out_dir, "summary_%s.csv" % config)
    df = pd.DataFrame(rows)
    df["Index"] = ["%s.%s.%s.%s" % (r["Protein"], r["Allele_ext"], r["Start"], r["End"]) for _, r in df.iterrows()]
    df.set_index("Index", drop=True, inplace=True)
    df.to_csv(fname, index=True, index_label="Index", header=first, mode="w" if first else "a")
    print("Written %d rows to %s" % (len(df), fname))
    if out_raw:
        for ae in set(df["Allele_ext"]):
            dft = df[df["Allele_ext"] == ae]
            fname = os.path.join(pep_dir, "%s_%s.pep" % (config, ae))
            dft[["Sequence"]].to_csv(fname, header=False, index=False, mode="w" if first else "a")
            print("Written %d rows to %s" % (len(dft), fname))
            fname = os.path.join(pep_dir, "%s_%s.faa" % (config, ae))
            write_fasta(fname, dft, col_name="Sequence", mode="w" if first else "a")
            print("Written %d rows to %s" % (len(dft), fname))
