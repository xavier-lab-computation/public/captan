import unittest
from captan.utils.tracks import *

class TestBio(unittest.TestCase):

    def test_transitions(self):
        x = np.array([1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1])
        t = tuple(sorted(transitions(x)))
        assert t == ((0, 2), (5, 8), (10, 11), (12, 13))

    def test_expand(self):
        x = np.array([1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1])
        e = expand(transitions(x), flank=2, L=len(x))
        t = tuple(sorted(e))
        assert t == ((0, 4), (3, 10), (8, 12), (11, 13))
        e = expand(transitions(x), flank=3, L=len(x))
        t = tuple(sorted(e))
        assert t == ((0, 5), (2, 10), (8, 12), (11, 13))

    def test_overlaps(self):
        """ Test representation of sequence as a tensor. """
        X = np.array([[0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0],
                      [0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0]])
        ovs = tuple(overlaps(X, flank=2))
        assert ovs == (((0, 1), REG_NFLANK), ((1, 4), REG_SECOND),
                       ((4, 6), REG_SHARED), ((6, 9), REG_FIRST), ((9, 11), REG_CFLANK))
        X = np.array([[0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                      [0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]])
        ovs = tuple(overlaps(X, flank=2))
        assert ovs == (((0, 1), REG_NFLANK), ((1, 4), REG_SECOND),
                       ((4, 6), REG_SHARED), ((6, 9), REG_FIRST), ((9, 10), REG_CFLANK),
                       ((10, 12), REG_SECOND), ((12, 14), REG_CFLANK),
                       ((15, 17), REG_NFLANK), ((17, 18), REG_SECOND), ((18, 20), REG_CFLANK))
        X = np.array([[0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0],
                      [0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0]])
        ovs = tuple(overlaps(X, flank=2))
        assert ovs == (((2, 4), REG_NFLANK), ((4, 9), REG_SHARED), ((9, 11), REG_CFLANK))
        # Overlaps extending
        X = np.array([[1,1,1,0,0,0,0,1,1,1,1],
                      [1,1,1,0,0,0,0,1,1,1,1]])
        ovs = tuple(overlaps(X, flank=8))
        assert ovs == (((0, 3), REG_SHARED), ((3, 7), REG_CFLANK), ((7, 11), REG_SHARED))

        # Overlaps extending
        X = np.array([[1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1],
                      [1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1]])
        ovs = tuple(overlaps(X, flank=8, offset=1))
        assert ovs == (((0, 3), REG_SHARED),
                       ((3, 6), REG_NFLANK),
                       ((4, 7), REG_CFLANK),
                       ((7, 11), REG_SHARED))
