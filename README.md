# Context-Aware Predictor of T-cell antigens (CAPTAn)

This package exposes the Context-Aware Predictor of T-cell antigens (CAPTAn), which implements predictions for 
T-cell epitopes, binding to 87 MHC-II human alleles.


## Docker image

The latest release of CAPTAn is hosted on dockerhub, including instructions to run the method.

<a href="https://hub.docker.com/r/cil6758/captan">CAPTAn docker image</a>.

## Installation from source

The recommended source installation consists of setting up a dedicated python virtual environment, e.g. in `~/py37captan`.

    virtualenv --python=python3.7 ~/py37captan
    source ~/py37captan/bin/activate

Alternatively in a conda environment 

    conda create -n captan37 python=3.7

Clone or unzip the package onto the user system, e.g. in `~/captan/`.

    git clone https://gitlab.com/xavier-lab-computation/public/captan.git ~/captan
    cd ~/captan/
    
Install package and its requirements (expected install time: 5-10 minutes).
    
    pip install -r requirements.txt 
    python setup.py install  # usage mode, ... 
    python setup.py develop  # ... or development mode

Optionally, test the configuration. There should be no errors if all packages installed correctly.

    python setup.py test

## Quick start

The suite consists of three modular steps:

1. Allele-resolution binding core predictions (`captan-core`)
2. Isotype-resolution epitope region prediction (`captan-context`)
3. Allele-specific weighted sum of steps 1 & 2 (`captan-merge`).  

### Example prediction with human coronaviruses

The following steps assume the virtual enviroment has been activated in the current terminal (see above).

Run prediction on SARS-Cov2 proteome.
    
    MODEL_CORE_DIR=./models/models_core
    MODEL_CTX_DIR="./models/models_context"
    IN_FA=./examples/hcov/fa/hcov.faa
    OUT=./examples/hcov/captan/

Predict for sets of alleles. The available set of MHC-II alleles can be seen in `./models/models_core/models.csv`.  For peptides of at least 9 amino acids, 
the most likely primary and secondary binding cores are extracted and scored with log2 likelihood (higher is better).

    captan-core --models $MODEL_CORE_DIR --input $IN_FA --output $OUT --alleles "DRB1*11:01" "DRB1_03_01" "DQA1*01:03,DQB1*06:03" --lengths 20 --details

Predictions using the context model.

    captan-context --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --isotypes "DR" "DQ" "DP" --threshold 0.2 --lengths 20 --details

Merged predictions for core and context models using local convolution. 
Both previous scripts `captan-core` and `captan-context` must have been run with `--details` flag on.
    
    captan-merge --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --details --lengths 20


### Example with mouse H-2-Iab allele.

The mouse version of MHC class II allele is the H-2-Iab allele, which is supported through training with immunopeptidomics data from 
<a href="https://www.nature.com/articles/s41591-018-0203-7">Graham, et. al 2020</a>. The syntax follows the above examples the context models use DQ isotype, with which H-2-Iab
shares the highest similarity.

    MODEL_CORE_DIR=./models/models_core
    MODEL_CTX_DIR="./models/models_context"
    IN_FA=./examples/hcov/fa/hcov.faa
    OUT=./examples/hcov/captan_mouse/
    captan-core --models $MODEL_CORE_DIR --input $IN_FA --output $OUT --alleles "H2-IAb" --lengths 20 --details
    captan-context --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --isotypes "DQ" --threshold 0.2 --lengths 20 --details
    captan-merge --models $MODEL_CTX_DIR --input $IN_FA --output $OUT --details --lengths 20




 
### Scoring a list of peptides of arbitrary length.

For a list of peptides (arbitrary lengths up to 50 aa), assign probabilities of peptides matching binding cores of the known alleles using `captan-core`.
This scripts avoids specifications of lengths a priori like `captan-core`, which operates on protein sequences.
It also computes and embedding for each peptide using the penultimate layer in the `captan-core` model, which can then be visualized with methods
such as <a href="https://opentsne.readthedocs.io/en/latest/">open t-SNE</a>. For peptides of at least 9 amino acids, 
the most likely primary and secondary binding cores are extracted and scored with log2 likelihood (higher is better).

    MODEL_DIR=./models/models_core/
    IN_FA=./examples/microbiome/peptides_dc.faa
    OUT=./examples/microbiome/captan/
    captan-score -a DRB1_03_01 -o $OUT -m $MODEL_DIR  -i $IN_FA


### Detailed options

Detailed settings `captan-core -h` : 

    usage: captan-core [-h] -i INP -o OUT -m MOD [-a ALLELES [ALLELES ...]]
                 [-A ALLELE_FILE] [-l LENGTHS [LENGTHS ...]] [-t THR] [-d]

    Predict probabilities of peptides matching binding cores of the known alleles.
    
    optional arguments:
      -h, --help            show this help message and exit
      -i INP, --input INP   Input fasta with amino acid sequences.
      -o OUT, --output OUT  Output directory.
      -m MOD, --models MOD  Directory with models.
      -a ALLELES [ALLELES ...], --alleles ALLELES [ALLELES ...]
                            Set of alleles to predict for.
      -A ALLELE_FILE, --allele-file ALLELE_FILE
                            Input .txt file with one allele per row
      -l LENGTHS [LENGTHS ...], --lengths LENGTHS [LENGTHS ...]
                            Peptide lengths to test
      -t THR, --threshold THR
                            Confidence threshold to summarize predictions
      -d, --details         Store detailed predictions and sequence files.

Detailed settings `captan-context -h` :

    usage: captan-merge [-h] -i INP -o OUT -m MOD [-l LENGTHS [LENGTHS ...]]
                  [-t THR] [-n NAME] [--name-core NAME_COR]
                  [--name-context NAME_CTX] [-d]

    Merge predictions from captan-core and captan-context models.
    
    optional arguments:
      -h, --help            show this help message and exit
      -i INP, --input INP   Input fasta with amino acid sequences.
      -o OUT, --output OUT  Output directory.
      -m MOD, --models MOD  Directory with context models.
      -l LENGTHS [LENGTHS ...], --lengths LENGTHS [LENGTHS ...]
                            Peptide lengths to test
      -t THR, --threshold THR
                            Confidence threshold to summarize predictions
      -n NAME, --name NAME  Configuration name.
      --name-core NAME_COR  Configuration name (core)
      --name-context NAME_CTX
                            Configuration name (context)
      -d, --details         Store detailed predictions and sequence files.

Detailed settings `captan-merge -h` :

    usage: captan-merge [-h] -i INP -o OUT -m MOD [-l LENGTHS [LENGTHS ...]]
                  [-t THR] [-n NAME] [--name-core NAME_COR]
                  [--name-context NAME_CTX] [-d]

    Merge predictions from captan-core and captan-context models.
    
    optional arguments:
      -h, --help            show this help message and exit
      -i INP, --input INP   Input fasta with amino acid sequences.
      -o OUT, --output OUT  Output directory.
      -m MOD, --models MOD  Directory with context models.
      -l LENGTHS [LENGTHS ...], --lengths LENGTHS [LENGTHS ...]
                            Peptide lengths to test
      -t THR, --threshold THR
                            Confidence threshold to summarize predictions
      -n NAME, --name NAME  Configuration name.
      --name-core NAME_COR  Configuration name (core)
      --name-context NAME_CTX
                            Configuration name (context)
      -d, --details         Store detailed predictions and sequence files.
      

Detailed settings `captan-score -h`.

    captan-score [-h] -i INP -o OUT -m MOD [-a ALLELES [ALLELES ...]]
                        [-A ALLELE_FILE] [-n NAME] [-s SEED]
    
    Predict probabilities of peptides matching binding cores of the known alleles.
    
    optional arguments:
      -h, --help            show this help message and exit
      -i INP, --input INP   Input fasta with amino acid sequences.
      -o OUT, --output OUT  Output directory.
      -m MOD, --models MOD  Directory with models.
      -a ALLELES [ALLELES ...], --alleles ALLELES [ALLELES ...]
                            Set of alleles to predict for.
      -A ALLELE_FILE, --allele-file ALLELE_FILE
                            Input .txt file with one allele per row
      -n NAME, --name NAME  Configuration name.
      -s SEED, --seed SEED  Random seed from {0, 1, 2, 3, 4}. Default is all.

## Integration testing

To test the complete configuration at once, run `setup-test.sh` within the
selected virtual environment. 

    source ~/py37captan/bin/activate
    ./setup-test.sh
    
The script tests the three main executables listed above, producing an output
similar to one below.

    Start
    Thu Mar 17 10:06:44 CET 2022
    captan-core --models ./models/models_core/ --input ./examples/hcov/fa/hcov.faa --output ./examples/hcov/captan/ --alleles DRB1*11:01 DRB1_03_01 DQA1*01:03,DQB1*06:03 --lengths 20 --details
    
    real	0m27.956s
    user	0m42.030s
    sys	0m4.389s
    Status: 0
    captan-context --models ./models/models_context/ --input ./examples/hcov/fa/hcov.faa --output ./examples/hcov/captan/ --isotypes DR DQ DP --threshold 0.2 --lengths 20 --details
    
    real	1m21.609s
    user	1m30.752s
    sys	0m2.943s
    Status: 0
    captan-merge --models ./models/models_context/ --input ./examples/hcov/fa/hcov.faa --output ./examples/hcov/captan/ --details --lengths 20
    
    real	0m8.600s
    user	0m5.845s
    sys	0m1.692s
    Status: 0
    End
    Thu Mar 17 10:08:42 CET 2022


### Version history

Versions denoted by git tags. 

* 2022_aug_review: Initial release.
* 2022_nov_release: Minor output formatting updates.
* 2022_dec_release: Add `captan-score` script.
* 2023_mar_release: Add binding core predictions.
* 2023_oct_release: Add mouse H-2-Iab models.
