import unittest
import keras
import numpy as np
import tensorflow as tf
from sklearn.metrics import roc_auc_score
from captan.utils.bio import load_amino_acids
from captan.utils.tracks import quantile, cut
from captan.models.context import LSTMeta


class TestLSTM(unittest.TestCase):


    def setUp(self):
        """ Generate data of arbitrary length. """

        # Report versions
        print("Tensorflow version: %s" % str(tf.__version__))
        print("Keras version: %s" % str(keras.__version__))

        # Generate data
        np.random.seed(41)
        N = 1000                     # Number of sequences
        d_out = 4                    # Number of outputs
        log_bounds = (2, 3)          # Length bounds
        p = 0.10                     # Probability of positive class
        motifs1 = ["TETAT", "TQTQT"]
        motifs2 = [["AAAAA", "AEAEA"],
                   ["AKAKE", "EKAKA"],
                   ["QEQEE", "QEQQE"],
                   ["AKQQA", "QQAAK"]] # Motifs to be inserted for each output, all of same length
        k = len(motifs1[0])
        offset = (-5, 5)

        # Output
        df = load_amino_acids()
        alphabet = df.index
        S = list()
        Y = list()

        # Gene sequences
        for j in range(N):
            ln = log_bounds[0] + np.random.rand() * (log_bounds[1] - log_bounds[0])
            n = round(10 ** ln)
            s = "".join(np.random.choice(alphabet, n, replace=True))
            Z = np.zeros((n, d_out))
            for ou in range(d_out):
                count = 0
                for i in range(0, n, k):
                    if np.random.rand() < p and i + k <= n and count < 5:
                        count += 1
                        Z[i: i + k, ou] = 1
                        m = np.random.choice(motifs2[ou])
                        s = s[:i] + m + s[i + k:]
                        off = np.random.choice(offset)
                        j = i + off
                        if j > 0 and j + k <= n:
                            m = np.random.choice(motifs1)
                            s = s[:j] + m + s[j + k:]
            assert Z.shape[0] == len(s)
            S.append(s)
            Y.append(Z)

        # Output
        self.S = S
        self.Y = Y
        self.d_out = d_out
        self.X = [np.ones((len(s), 1)) for s in S]

    def test_list2tensor(self):
        """ Test list2tensor"""
        aa = load_amino_acids()
        settings = {"alphabet": aa.index, "hidden": 10, "filters": 4, "filters_depth": 3,
                    "dropout": 0, "activation": "tanh", "bidirectional": True,
                    "conv_kernel": 8, "conv_filters": 10,
                    "n_threads": 2, "d_in": 1, "d_out": 3,
                    "family": LSTMeta.FAMILY_LSTM}
        S = ["AAA", "CCCC", "MMMMM", "NNNNN"]
        X = [np.ones((len(s), 1)) for s in S]
        Y = [np.zeros((len(s), 3)) for s in S]
        for i in range(len(Y) -1 ):
            Y[i][:2, 0] = 1
            Y[i][:1, 1] = 1
        model = LSTMeta(**settings)
        Ts, Tx, Z, W = model.lists2tensors(S, X, Y, prior=.5)
        assert len(Ts) == len(Tx) == len(Z) == len(W) == len(S)

    def test_lstm_joint_prediction(self):
        """ Test pre-training for LSTM"""
        aa = load_amino_acids()
        settings = {"alphabet": aa.index, "hidden": 10, "filters": 4, "filters_depth": 3,
                    "dropout": 0, "activation": "tanh", "bidirectional": True,
                    "conv_kernel": 8, "conv_filters": 10,
                    "n_threads": 2, "d_in": 1,
                    "family": LSTMeta.FAMILY_LSTM}
        self.prediction(settings)

    def prediction(self, settings):
        """ Test LSTM with sequences of arbitrary length. Include metadata with sequences.
            Assert that model is able to pick out the correct signal from metadata even
            if sequences are shuffled.
        """
        S = self.S
        Y = self.Y
        X = self.X
        d_out = Y[0].shape[-1]
        epochs = 5
        (S, X, Y), (S_va, X_va, Y_va) = LSTMeta.split(S, X, Y, split=.3)
        rnn = LSTMeta(d_out=d_out, **settings)
        rnn.model.summary()
        rnn.fit(S, X, Y, epochs = epochs, validation_split= 0.2, n_batches=64, verbose=True)
        Yp = rnn.predict(S_va, X_va)
        y_tot = list()
        yp_tot = list()
        lengths = list(map(len, S_va))
        pc = quantile(lengths)
        qe = cut(lengths, pc)
        yp_means = dict()
        y_means = dict()
        for s, x, y, yp, lq in zip(S_va, X_va, Y_va, Yp, qe):
            y_tot.extend(y.ravel())
            yp_tot.extend(yp.ravel())
            y_means[lq] = y_means.get(lq, []) + [np.mean(y)]
            yp_means[lq] = yp_means.get(lq, []) + [np.mean(yp[y==0])]
            assert y.shape == yp.shape
        auc = roc_auc_score(y_tot, yp_tot)
        print("LSTM validation AUC: %.3f" % auc)
        for lq in sorted(yp_means.keys()):
            print("Length quantile: %s-%s \t obs. / pred. nz. mean: \t %.3f %.3f" %
                  (lq[0], lq[1], float(np.mean(y_means[lq])), float(np.mean(yp_means[lq]))))
        assert auc > .5
        # Predict motifs
        M = rnn.predict_motifs(S_va)
        assert len(M) == len(S_va)
        assert M[0].shape == (len(S_va[0]), settings["conv_filters"])


if __name__ == "__main__":
    unittest.main()
